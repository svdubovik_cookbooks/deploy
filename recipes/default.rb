#
# Cookbook:: deploy
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

node.default['deploy']['path'] = '/var/www/html/wp-content'
node.default['deploy']['git_repo'] = 'https://gitlab.com/svdubovik/test-wordpress.git'
# node.default['deploy']['env'] = ''

# deploy = search(:deploys, "id:#{node['deploy']['env']}").first
deploy = search(:deploys, "id:#{node.chef_environment}").first

git node['deploy']['path'] do
  repository node['deploy']['git_repo']
  revision deploy['commit']
  checkout_branch deploy['branch']
  enable_checkout false
  action :sync
end
